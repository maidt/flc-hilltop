<?php wp_footer(); ?>
<footer>
    <?php
        $footer = get_field('footer', 'option');
    ?>
    <img class="_decor_left" src="<?php echo IMAGE_URL .'/home_mai/footer_decor2.png'?>" alt="">
    <div class="_drums">
        <img src="<?php echo IMAGE_URL . '/homes/drums.png' ?>" alt="">
    </div>
    <div class="_ft_top">
        <div class="_item">
            <a href="#"><img class="_logo" src="<?php echo IMAGE_URL .'/home_mai/logo_ft.png'?>" alt=""></a>
            <p><span>Địa chỉ dự án:</span> <?php echo $footer['address_project']; ?></p>
            <p><span>Hotline:</span><a href="tel: <?php echo $footer['hotline_project']; ?>"> <?php echo $footer['hotline_project']; ?></a></p>
        </div>
        <div class="_item">
            <div class="_logo_other">
                <?php foreach ($footer['logo_group'] as $gallery) { ?>
                    <a href="#"><img src="<?php echo $gallery ?>" alt=""></a>
                <?php } ?>
            </div>
            <p style="text-transform: uppercase;"><?php echo $footer['name_company']; ?></p>
            <p><span>Địa chỉ:</span> <?php echo $footer['address_company']; ?></p>
        </div>
        <div class="_item">
            <div class="_title">Lưu ý</div>
            <p><?php echo $footer['note']; ?></p>
            <div class="_title _bottom">Kết nối với chúng tôi</div>
            <ul class="_social">
                <li><a href="<?php echo $footer['link_facebook']; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="<?php echo $footer['link_youtube']; ?>"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
            </ul>
        </div>
    </div>
    <div class="_ft_bottom">Copyright © FLC Hilltop Gilai 2019. All right reserved.</div>
</footer>
<?php include_once(get_template_directory() . '/partials/popup_register.php'); ?>
</body>
<script>
    jQuery(document).ready(function ($) {
        if ( $('body').width() > 1024 ) { 
            AOS.init({
                delay: 0,
                duration: 1000, 
                easing: 'ease', 
                once: true, 
                mirror: false,
                offset: 100,
                easing: 'ease-in-out',
            });
        }else {
            AOS.init({
                disable: true
            });
        } 
        $('._register_buy_m, ._register_buy').click(function() {
           $('body').addClass('not_scroll');
        });
        $('.js_popup_register ._close').click(function() {
            $('body').removeClass('not_scroll');
        });
    });
</script>
</html>
