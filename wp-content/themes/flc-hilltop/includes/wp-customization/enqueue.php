<?php

/**
 * Adding scripts and styles
 * All your scripts and styles will be included in wp_head()
 */
add_action('wp_enqueue_scripts', 'tu_enqueue_scripts_styles');

function tu_enqueue_scripts_styles() {
    
    if (wp_script_is('media')) {
        wp_enqueue_media();
    }

    wp_enqueue_style('font-awesome', TEMPLATE_URL.'/assets/bower_components/font-awesome/css/font-awesome.min.css');
    wp_enqueue_style('WOW', TEMPLATE_URL.'/assets/bower_components/WOW/css/libs/animate.css');
    wp_enqueue_style('fancybox-style', TEMPLATE_URL.'/assets/bower_components/fancybox/dist/jquery.fancybox.min.css');
    wp_enqueue_style('swiper-style', TEMPLATE_URL . '/assets/bower_components/Swiper/dist/css/swiper.min.css');
    wp_enqueue_style('aos-style', TEMPLATE_URL . '/assets/bower_components/aos/dist/aos.css');
     wp_enqueue_style('msroll-style', TEMPLATE_URL . '/assets/bower_components/jquery.mCustomScrollbar/jquery.mCustomScrollbar.css');
    wp_enqueue_style('partials', TEMPLATE_URL.'/assets/stylesheets/sass/dist/partials.css');
    wp_enqueue_style('main', TEMPLATE_URL.'/assets/stylesheets/sass/dist/main.css');

    wp_enqueue_script('jquery', TEMPLATE_URL . '/assets/bower_components/jquery/src/jquery.js', array(), '0.1', false);
    wp_enqueue_script('jquery-js', TEMPLATE_URL . '/assets/bower_components/jquery/dist/jquery.min.js', array(), '0.1', false);
    wp_enqueue_script('fancybox-js', TEMPLATE_URL . '/assets/bower_components/fancybox/dist/jquery.fancybox.min.js', array(), '0.1', false);
    wp_enqueue_script('WOW-js', TEMPLATE_URL . '/assets/bower_components/WOW/dist/wow.min.js', array(), '0.1', false);
    wp_enqueue_script('panzoom', TEMPLATE_URL . '/assets/bower_components/jquery.panzoom/dist/jquery.panzoom.min.js', array(), '0.1', false);
    wp_enqueue_script('swiper-js', TEMPLATE_URL . '/assets/bower_components/jquery.mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js', array(), '0.1', false);
    wp_enqueue_script('msroll-js', TEMPLATE_URL . '/assets/bower_components/Swiper/dist/js/swiper.min.js', array(), '0.1', false);
    wp_enqueue_script('aos', TEMPLATE_URL . '/assets/bower_components/aos/dist/aos.js', array(), '0.1', false);
    wp_enqueue_script('main', TEMPLATE_URL . '/assets/scripts/main.js', array(), '0.1', false);

    $wp_script_data = array(
        'ADMIN_AJAX_URL' => ADMIN_AJAX_URL,
        'HOME_URL' => HOME_URL
    );

    wp_localize_script('main', 'wp_vars', $wp_script_data);
}



