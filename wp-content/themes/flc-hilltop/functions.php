<?php

/**
 * Define constants
 * These constants will be used globally
 */
define('BLOG_NAME', get_option('blogname'));
define('HOME_URL', home_url('/'));
define('TEMPLATE_URL', get_template_directory_uri());
define('TEMPLATE_PATH', get_template_directory());
define('ADMIN_AJAX_URL', admin_url('admin-ajax.php'));
define('IMAGE_URL', TEMPLATE_URL . '/assets/images');
define('NO_IMAGE_URL', IMAGE_URL.'/no-image.png');
define('TEXT_DOMAIN', 'tu');

/**
 * Constants for configuration
 */
define('FACEBOOK_APP_ID', '471714836676097');

/**
 * Including core stuffs
 */
include_once(TEMPLATE_PATH . '/includes/init.php');

/**
 * Including post-types files
 * You can create more post-types if you need but you should use the structure of existed files
 */
include_once(TEMPLATE_PATH . '/post-types/contact.php');
include_once(TEMPLATE_PATH . '/post-types/article.php');


/**
 * Including option page
 */
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title'    => 'Quản lý chung',
        'menu_title'    => 'Quản lý chung',
        'menu_slug'     => 'theme-general-settings',
        'capability'    => 'edit_posts',
        'position'      =>  '3.5',
        'icon_url'      => 'dashicons-admin-home',
        'redirect'      => false
    ));
}