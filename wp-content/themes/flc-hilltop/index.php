<?php get_header(); ?>
	<main style="overflow: hidden">
		<div class="homes_page">
			<?php include_once(get_template_directory() . '/partials/homes/section_1.php'); ?>
			<?php include_once(get_template_directory() . '/partials/homes/section_2.php'); ?>
			<?php include_once(get_template_directory() . '/partials/homes/section_3.php'); ?>
			<?php include_once(get_template_directory() . '/partials/homes/section_4.php'); ?>
			<?php include_once(get_template_directory() . '/partials/homes/section_5.php'); ?>
			<?php include_once(get_template_directory() . '/partials/homes/section_6.php'); ?>
			<?php include_once(get_template_directory() . '/partials/homes/section_7.php'); ?>
			<?php include_once(get_template_directory() . '/partials/homes/section_8.php'); ?>
			<?php include_once(get_template_directory() . '/partials/homes/section_9.php'); ?>
		</div>
	</main>
	<script>
	  	jQuery(document).ready(function ($) {

	  		if ( $('body').width() > 1024 ) { 
	        	AOS.init({
	        		offset: 200,
			        duration: 850,
			        easing: 'ease-in-out',
	        	});
	        }else {
	        	AOS.init({
					disable: true
				});
	        };
	   	});
	</script>
<?php get_footer(); ?>