<?php $_siteInfo = array(
    'app_id' => FACEBOOK_APP_ID,// App Name: TU
    'type' => 'website',// Need to be changed with each type of pages
    'title' => get_bloginfo('name'),// Site Title
    'url' => home_url(),// Permalink
    'image' => get_template_directory_uri() . '/screenshot.jpg?v='.date('dmY'),// Screenshot, Thumbnail
    'description' => get_bloginfo('description'),// Tagline, Excerpt
    'author' => 'Time Universal',// Change manually
);

if( is_tax() || is_category() || is_tag() ) {
    if( !is_category() )
        $_siteInfo['url'] = get_term_link( get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
    else
        $_siteInfo['url'] = get_category_link( get_query_var( 'cat' ) );

    $_siteInfo['title'] = single_term_title( '', false ) .' | '. get_bloginfo('name');
    $_siteInfo['description'] = term_description() ? term_description() : $_siteInfo['description'];
    $_siteInfo['type'] = 'article';
}

if( is_search() ) {
    $_siteInfo['title'] = 'Search: '. esc_html( get_query_var('s') ) .' | '. get_bloginfo('name');
    $_siteInfo['description'] = 'Search result for "'. esc_html( get_query_var('s') ) .'" from '. get_bloginfo('name');
}

if( is_author() ) {
    $authorID = get_query_var('author');
    $authorData = get_userdata( $authorID );
    $_siteInfo['title'] = $authorData->display_name .' @ '. get_bloginfo('name');
    $_siteInfo['description'] = $authorData->description ? $authorData->description : $_siteInfo['description'];
}

if( is_single() || is_page() ) {
    $imageSource = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
    $postDescription = $post->post_excerpt ? $post->post_excerpt : wp_trim_words( $post->post_content );
    $_siteInfo['title'] = $post->post_title;
    $_siteInfo['description'] = $postDescription ? strip_tags( $postDescription ) : $_siteInfo['description'];
    $_siteInfo['image'] = $imageSource ? $imageSource[0] : $_siteInfo['image'];
    $_siteInfo['url'] = get_permalink( $post->ID );
    $_siteInfo['type'] = 'article';
    // $_siteInfo['author'] = get_the_author_meta( 'display_name', $post->post_author );
}

if( is_paged() ) {
    $_siteInfo['title'] .= ' | '.__('Trang', TEXT_DOMAIN).' '. get_query_var('paged');
    $_siteInfo['description'] .= ' | '.__('Trang', TEXT_DOMAIN).' '. get_query_var('paged');
}
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <meta name="author" content="<?php echo str_replace('"', '', $_siteInfo['author']);?>" />
    <meta name="description" content="<?php echo str_replace('"', '', $_siteInfo['description']);?>" />
    <meta property="fb:app_id" content="<?php echo $_siteInfo['app_id'];?>" />
    <meta property="og:type" content='<?php echo $_siteInfo['type'];?>' />
    <meta property="og:title" content="<?php echo str_replace('"', '', $_siteInfo['title']);?>" />
    <meta property="og:url" content="<?php echo $_siteInfo['url'];?>" />
    <meta property="og:image" content="<?php echo $_siteInfo['image'];?>" />
    <meta property="og:description" content="<?php echo str_replace('"', '', $_siteInfo['description']);?>" />
    <link type="image/x-icon" rel="shortcut icon" href="<?php echo IMAGE_URL; ?>/favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
    <title><?php echo esc_html( $_siteInfo['title'] ); ?></title>

    <?php wp_head();?>
    
    <!-- Admicro Tag Manager -->
    <script> (function(a, b, d, c, e) { a[c] = a[c] || [];
    a[c].push({ "atm.start": (new Date).getTime(), event: "atm.js" });
    a = b.getElementsByTagName(d)[0]; b = b.createElement(d); b.async = !0;
    b.src = "//deqik.com/tag/corejs/" + e + ".js"; a.parentNode.insertBefore(b, a)
    })(window, document, "script", "atmDataLayer", "ATMG1W7NC2KZ6");</script>
    <!-- End Admicro Tag Manager -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?..."></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-154561818-1');
    </script>
</head>
<body <?php body_class(); ?>>
    <div id="fb-root"></div>

    <script>
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.11";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <?php if( is_home() ): ?>
    <?php endif; ?>
    <?php
        $footer = get_field('footer', 'option');
    ?>
    <header>
        <div class="_header">
            <div class="__logo_project" data-aos="fade-down">
                <a href="<?php echo bloginfo('url') ?>"><img src="<?php echo IMAGE_URL .'/homes/logo.png'?>" alt=""></a>
            </div>
            <div class="__menu" data-aos="fade-down">
                <ul>
                    <?php tu_nav_menu('main-menu'); ?>
                    <!-- <li data-aos="fade-right" data-aos-delay="200" class="current-menu-parent"><a href="#">trang chủ</a></li>
                    <li data-aos="fade-right" data-aos-delay="300" data-src=".section_2"><a href="javascript:void(0);">giới thiệu</a></li>
                    <li data-aos="fade-right" data-aos-delay="400"><a href="<?php echo bloginfo('url') ?>/san-pham">Sản phẩm</a></li>
                    <li data-aos="fade-right" data-aos-delay="500" data-src=".section_5"><a href="javascript:void(0);">tiện ích</a></li>
                    <li data-aos="fade-right" data-aos-delay="600" data-src=".section_6"><a href="javascript:void(0);">thư viện</a></li>
                    <li data-aos="fade-right" data-aos-delay="700"><a href="<?php echo bloginfo('url') ?>/tin-tuc">tin tức</a></li> -->
                </ul>
            </div>
            <div class="_register_buy" data-aos="zoom-in" data-aos-delay="700">
                <span>Đăng ký mua</span>
            </div>

            <div class="_social_m">
                <div class="_txt">Theo dõi chúng tôi:</div>
                <div class="_list_icon">
                    <a href="<?php echo $footer['link_facebook'] ?>"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                    <a href="mailto: <?php echo $footer['link_email'] ?>"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </header>
    <div class="_header_mobile">
        <div class="_item">
            <div class="_pull js_change">
                <div></div>
                <div></div>
                <div></div>
            </div>
            <div class="_txt">
                Menu
            </div>
        </div>
        <div class="_item">
            <a href="tel: <?php echo $footer['hotline_project'] ?>">
                <img src="<?php echo IMAGE_URL .'/homes/phone_m.png'?>" alt="">
                <div class="_txt">Hotline</div>
            </a>
        </div>
        <div class="_item">
            <a href="javascript:void(0);" class="_register_buy_m">
                <img src="<?php echo IMAGE_URL .'/homes/regis_m.png'?>" alt="">
                <div class="_txt">Đăng ký mua</div>
            </a>
        </div>
    </div>
    <div class="_coating_m"></div>
    <div class="_logo_mobile">
        <a href="<?php echo bloginfo('url') ?>"><img src="<?php echo IMAGE_URL .'/homes/logo.png'?>" alt=""></a>
    </div>

    <script type="text/javascript">
        jQuery(document).ready(function($){
            $('._register_buy, ._register_buy_m').click(function(e){
                $('.js_popup_register').addClass('is_open');
            })
            $('.js_popup_register ._close').click(function(e){
                $('.js_popup_register').removeClass('is_open');
            })
            if($(window).width() >= 1024){
                var iScrollPos = 0;
                $(window).scroll(function () {
                    var iCurScrollPos = $(this).scrollTop();
                    if (iCurScrollPos < iScrollPos) {
                        $('header').removeClass('scroll_hide');
                        $('header').addClass('is_active');
                    } else {
                        if($(this).scrollTop()==0){
                            $('header').removeClass('scroll_hide');
                            $('header').removeClass('is_active');
                        }else{
                            $('header').addClass('scroll_hide');
                            $('header').removeClass('is_active');
                        }
                    };
                    if($(this).scrollTop() <= 0){
                        $('header').removeClass('is_active');
                        $('header').removeClass('hide_header');
                    }
                    iScrollPos = iCurScrollPos;
                });
            }
            if($(window).width() <= 768){
                $('.js_change').click(function(e){
                    $(this).toggleClass('__change');
                    $(this).parent('._item').toggleClass('__active');
                    $('header, ._coating_m').toggleClass('__active');
                });
                $('._coating_m').click(function(e){
                    $('.js_change').toggleClass('__change');
                    $('.js_change').parent('._item').toggleClass('__active');
                    $('header, ._coating_m').toggleClass('__active');
                });

                var iScrollPos = 0;
                $(window).scroll(function () {
                    var iCurScrollPos = $(this).scrollTop();
                    if (iCurScrollPos < iScrollPos) {
                        $('._logo_mobile').removeClass('scroll_hide');
                        $('._logo_mobile').addClass('is_active');
                    } else {
                        if($(this).scrollTop()==0){
                            $('._logo_mobile').removeClass('scroll_hide');
                            $('._logo_mobile').removeClass('is_active');
                        }else{
                            $('._logo_mobile').addClass('scroll_hide');
                            $('._logo_mobile').removeClass('is_active');
                        }
                    };
                    if($(this).scrollTop() <= 0){
                        $('._logo_mobile').removeClass('is_active');
                        $('._logo_mobile').removeClass('hide_header');
                    }
                    iScrollPos = iCurScrollPos;
                });

            };

            // var flag = $('body').hasClass('home');
            // if(flag){
            //     $('[data-src]').click(function(e){
            //         var this_scroll = $(this).attr('data-src');
            //         var el_scroll = $(''+this_scroll+'');
            //         $('html, body').animate({
            //             scrollTop: $(el_scroll).offset().top
            //         }, 1000)
            //     });
            // }
            
        });
    </script>
