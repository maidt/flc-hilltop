<?php //Template Name: Agency
    get_header();
?>
<?php
    $banner = get_field('img_banner');
    $label = get_field('label');
    $content = get_field('content');
    $ground = get_field('img_ground');
    $utilities = get_field('utilities');
    $img_utilities = get_field('img_utilities');
?>
<div class="banner_page" style="background-image: url('<?php echo $banner; ?>')">
    <div class="_decor">
        <img src="<?php echo IMAGE_URL .'/homes/decor_banerpage.png' ?>" alt="">
    </div>
    <h2><?php echo $label; ?></h2>
</div>

<div class="agency">
    <div class="_txt">
       <?php echo $content; ?>
    </div>
    <div class="_utilities">
        <div class="__list">
            <ul>
                <?php foreach ($utilities as $list) { ?>
                    <li data-number="<?php echo $list['number']; ?>"><span><?php echo $list['number']; ?></span><span><?php echo $list['name']; ?></span></li>
                <?php } ?>
            </ul>
        </div>
        <div class="__img js_scroll_utili">
            <img src="<?php echo $ground; ?>" alt="">
            <div class="__dot">
                <?php foreach ($utilities as $list) { ?>
                    <div class="__item" data-number="<?php echo $list['number']; ?>">
                        <img src="<?php echo IMAGE_URL .'/homes/dot_uliti.png' ?>" alt="">
                        <div class="_number"><?php echo $list['number']; ?></div>
                        <div class="_note">
                            <?php echo $list['name']; ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <a href="<?php echo IMAGE_URL . '/homes/agency_img.png' ?>" class="_zoom" data-fancybox>
                <i class="fa fa-search" aria-hidden="true"></i>
            </a>
            <div class="_drums">
                <img src="<?php echo IMAGE_URL . '/homes/drums_utili.png' ?>" alt="">
            </div>
        </div>
    </div>

    <div class="image_utili">
        <div class="_title">
            Ảnh tiện ích
        </div>
        <div class="_slide">
            <div class="swiper-container js_slide_utili">
                <div class="swiper-wrapper">
                    <?php foreach ($img_utilities as $list) { ?>
                        <div class="swiper-slide">
                            <div class="_img" style="background-image:url('<?php echo $list['img']; ?>')"></div>
                            <div class="_content">
                                <h4><?php echo $list['name']; ?></h4>
                                <p><?php echo $list['description']; ?></p>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <!-- <div class="swiper-pagination"></div> -->
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
            <div class="__content js_adarp_text">
            </div>
            <div class="swiper-pagination js_swiper_pagination"></div>
        </div>
    </div>
</div>
<?php include_once(get_template_directory() . '/partials/homes/section_8.php'); ?>
<?php include_once(get_template_directory() . '/partials/social.php'); ?>
<script>
    jQuery(document).ready(function($){
        $('[data-number]').on('click', function(e){
            var data_src = $(this).attr('data-number');
            var data_src_active = $('[data-number = "'+data_src+'"]');
            $(data_src_active).siblings().removeClass('is_active');
            $(data_src_active).addClass('is_active');
        });
        $('li[data-number]').on('click', function(e){
            $('html, body').animate({
                scrollTop: $('.js_scroll_utili').offset().top
            }, 1000)
        });

        var swiper_utili = new Swiper('.swiper-container.js_slide_utili', {
            effect: 'coverflow',
		    grabCursor: true,
		    centeredSlides: true,
	        loop: true,
	        slidesPerView: 'auto',
	        speed: 1200,
	        coverflowEffect: {
		        rotate: 0,
		        stretch: 0,
		        depth: 300,
		        modifier: 1,
		        slideShadows : true,
		    },
            pagination: {
                el: '.js_swiper_pagination.swiper-pagination',
                type: 'fraction',
            },
            navigation: {
                nextEl: '.js_slide_utili .swiper-button-next',
                prevEl: '.js_slide_utili .swiper-button-prev',
            },
        });

        $('.js_adarp_text').text($('.js_slide_utili .swiper-slide.swiper-slide-active ._content p').text());
        swiper_utili.on('slideChange', function(){
            setTimeout(() => {
                $('.js_adarp_text').text($('.js_slide_utili .swiper-slide.swiper-slide-active ._content p').text());
            }, 500);
        });
    });
</script>
<?php get_footer(); ?>