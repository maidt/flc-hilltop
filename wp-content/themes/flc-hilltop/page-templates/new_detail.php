<?php //Template Name: New detail
    get_header();
?>
<div class="banner_page" style="background-image: url('<?php echo IMAGE_URL .'/homes/banner_page.png' ?>')">
    <div class="_decor">
        <img src="<?php echo IMAGE_URL .'/homes/decor_banerpage.png' ?>" alt="">
    </div>
    <h2>Tin tức</h2>
</div>
<div class="new_detail">
    <div class="_left">
        <div class="_time"><img src="<?php echo IMAGE_URL .'/homes/clock.png' ?>" alt=""><span>02/12/2019</span></div>
        <h2>Gia Lai trao chứng nhận đầu tư cho nhiều dự án công nghệ, đô thị, du lịch</h2>
        <div class="_excerpt">
            Trong khuôn khổ sự kiện Trình diễn và Kết nối cung – cầu công nghệ (Techdemo) 2019 tại TP Pleiku, UBND tỉnh Gia Lai đã trao chứng nhận đầu tư, biên bản ghi nhớ và cam kết đầu tư cho nhiều dự án quy mô lớn.
        </div>
        <div class="_the_content">
            <p>
                Với chủ đề “Kết nối công nghệ, bắt nhịp cuộc cách mạng công nghiệp 4.0”, Techdemo 2019 diễn ra từ 24 – 26/11 tại Gia Lai. Sự kiện thu hút gần 500 gian trưng bày, trình diễn công nghệ; 120 gian trưng bày sản phẩm đặc thù của tỉnh; 180 gian trưng bày sản phẩm của các doanh nghiệp, khu tư vấn công nghệ cải tiến kỹ thuật và các chương trình kết nối tọa đàm chuyên sâu giới thiệu tiềm năng, thế mạnh của Gia Lai nhằm thu hút các doanh nghiệp đẩy mạnh đầu tư trên địa bàn tỉnh.
            </p>
            <div class="wp-caption">
                <img src="<?php echo IMAGE_URL .'/homes/new_img_detail.png' ?>" alt="">
                <p class="wp-caption-text">
                    Lễ khai mạc sự kiện Techdemo 2019
                </p>
            </div>
            <p>
                Phát biểu tại lễ khai mạc, Phó Thủ tướng thường trực Chính phủ Trương Hòa Bình ghi nhận và đánh giá cao việc tổ chức Techdemo 2019 tại Gia Lai thuộc khu vực miền Trung – Tây Nguyên, nơi có vị trí chiến lược quan trọng đối với cả nước.
            </p>
            <p>
                TechDemo 2019 tập trung giải quyết các vấn đề thực tiễn hoạt động trong giai đoạn mới để đánh thức tiềm năng doanh nghiệp khu vực Tây Nguyên nói chung và cả nước nói riêng, đưa Gia Lai trở thành thủ phủ mới của ngành công nghiệp năng lượng tái tạo.
            </p>
            <div class="wp-caption">
                <img src="<?php echo IMAGE_URL .'/homes/new_img_detail.png' ?>" alt="">
                <p class="wp-caption-text">
                    Lễ khai mạc sự kiện Techdemo 2019
                </p>
            </div>
                <p>
                    Cũng tại sự kiện, trước sự chứng kiến của Phó Thủ tướng thường trực Chính phủ, UBND tỉnh Gia Lai đã trao giấy chứng nhận đầu tư cho 5 dự án thuộc các lĩnh vực công nghệ, đô thị, du lịch với tổng số vốn đầu tư 2.430 tỷ đồng, 6 dự án đăng ký ghi nhớ với tổng số vốn đầu tư lên tới 17.500 tỷ đồng.
                </p>
                <p>
                    Trong đó, Tập đoàn FLC gây chú ý khi nhận chứng nhận đầu tư cho dự án Tổ hợp khách sạn, nhà phố thương mại nằm tại trung tâm thành phố Pleiku với tổng vốn đầu tư trên 760 tỷ đồng. Một dự án khác được FLC và UBND tỉnh Gia Lai ký biên bản ghi nhớ hợp tác đầu tư là dự án Khu du lịch văn hóa Cao Nguyên Đồi thông kết hợp đô thị sinh thái quy mô 128 ha nằm trên ranh giới huyện Ia Grai và thành phố Pleiku… 
                </p>
        </div>
    </div>
    <div class="_right">
        <div class="_see_agency">
            <a href="#">
                <img src="<?php echo IMAGE_URL .'/homes/new_agency.png' ?>" alt="">
                <p class="_txt">Xem mặt bằng dự án</p>
            </a>
        </div>

        <div class="_video_project">
            <div class="_title">
                Video dự án
            </div>
            <div class="_content">
                <img src="<?php echo IMAGE_URL .'/homes/s3_img.png' ?>" alt="">
                <a class="__play" data-fancybox href="https://youtu.be/tPFP0gykLUY">
                    <img src="<?php echo IMAGE_URL . '/homes/play.png' ?>" alt="">
                </a>
            </div>
        </div>
        <div class="_related_news">
            <div class="_title">Các bài viết khác</div>
            <div class="_list_news">
                <a href="#">
                    <div class="_img" style="background-image: url('<?php echo IMAGE_URL . '/homes/news_img2.png' ?>')"></div>
                    <div class="_txt">
                        <h3>Gia Lai trao chứng nhận đầu tư cho nhiều dự án công nghệ, đô thị, du lịch</h3>
                        <p>Xem chi tiết</p>
                    </div>
                </a>
                <a href="#">
                    <div class="_img" style="background-image: url('<?php echo IMAGE_URL . '/homes/news_img2.png' ?>')"></div>
                    <div class="_txt">
                        <h3>Gia Lai trao chứng nhận đầu tư cho nhiều dự án công nghệ, đô thị, du lịch</h3>
                        <p>Xem chi tiết</p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
<?php include_once(get_template_directory() . '/partials/homes/section_8.php'); ?>
<?php include_once(get_template_directory() . '/partials/social.php'); ?>
<?php get_footer(); ?>