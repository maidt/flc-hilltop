<?php
    //  Template Name: Thank you
?>
<style>
    footer, ._header_mobile, ._logo_mobile{
        display: none !important;
    }
    header{
        opacity: 0;
        visibility: hidden;
    }
</style>
    <div class="thank_you">
        <div class="_left" style="background-image: url('<?php echo IMAGE_URL .'/homes/tk_img.png' ?>')"></div>
        <div class="_right">
            <div class="_logo_tk">
                <a href="<?php echo bloginfo('url') ?>"><img src="<?php echo IMAGE_URL .'/homes/logo_tk.png'?>" alt=""></a>
            </div>
            <div class="_title">
                <h3>Cảm ơn bạn đã gửi <br>thông tin cho chúng tôi</h3>
                <p>Chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất</p>
            </div>
            <a href="<?php echo bloginfo('url') ?>" class="_button">Trở lại trang chủ</a>
            <div class="_drump">
                <img src="<?php echo IMAGE_URL .'/homes/drum_tk.png'?>" alt="">
            </div>
        </div>
    </div>
<?php
    get_header();
?>