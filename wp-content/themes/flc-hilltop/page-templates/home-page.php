<?php //  Template Name: Home page ?>

<?php get_header(); ?>
	<main>
		<div class="homes_page">
			<?php include_once(get_template_directory() . '/partials/homes/section_1.php'); ?>
			<?php include_once(get_template_directory() . '/partials/homes/section_2.php'); ?>
			<?php include_once(get_template_directory() . '/partials/homes/section_3.php'); ?>
			<?php include_once(get_template_directory() . '/partials/homes/section_4.php'); ?>
			<?php include_once(get_template_directory() . '/partials/homes/section_5.php'); ?>
			<?php include_once(get_template_directory() . '/partials/homes/section_6.php'); ?>
			<?php include_once(get_template_directory() . '/partials/homes/section_7.php'); ?>
			<?php include_once(get_template_directory() . '/partials/homes/section_8.php'); ?>
			<?php include_once(get_template_directory() . '/partials/homes/section_9.php'); ?>
			<?php include_once(get_template_directory() . '/partials/social.php'); ?>
		</div>
	</main>
	<script>
	  	jQuery(document).ready(function ($) {
			// new WOW().init();
			wow = new WOW(
				{
				boxClass:     'wow',     
				animateClass: 'animated',
				offset:       0,         
				mobile:       false,      
				live:         false       
			}
			)
			wow.init();
	   	});
	</script>
<?php get_footer(); ?>
