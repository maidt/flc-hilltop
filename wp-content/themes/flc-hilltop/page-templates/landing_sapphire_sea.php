<?php
    //  Template Name: Landing sapphire sea
?>
<?php get_header(); ?>

<div class="_landing_sapphire_sea">
    <section class="sps_s1">
        <div class="_slide_img">
            <div class="swiper-container js_slide_sps_s1">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img src="<?php echo IMAGE_URL . '/sapphire_sea/s1_img1.jpg'?>">
                        <div class="_text_general">
                            <p>VAY TỐI ĐA <span class="_big">70%</span> <span class="_small">GTCH</span></p>
                            <p>HỖ TRỢ LÃI SUẤT 0% ĐẾN 31/03/2020</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <img src="<?php echo IMAGE_URL . '/sapphire_sea/s1_img2.jpg'?>">
                    </div>
                    <div class="swiper-slide">
                        <img src="<?php echo IMAGE_URL . '/sapphire_sea/s1_img3.jpg'?>">
                    </div>
                </div>
            </div>
            
        </div>
        <div class="_content_general">          
            <div class="swiper-pagination panigation_def"></div>
            <div class="_intro_general">
                <div class="_item">
                    <div class="_icon"><img src="<?php echo IMAGE_URL . '/sapphire_sea/s1_icon1.png'?>"></div>
                    <div class="_text">
                        <p>chỉ cần thanh toán</p>
                        <p>10%</p>
                        <p>Giá trị căn hộ <br>Ký ngay HĐMB</p>
                    </div>
                </div>
                <div class="_item">
                    <div class="_icon"><img src="<?php echo IMAGE_URL . '/sapphire_sea/s1_icon2.png'?>"></div>
                    <div class="_text">
                        <p>Chiết khấu lên đến</p>
                        <p>8%<span>/năm</span></p>
                        <p>Trên dòng tiền <br>thanh toán sớm</p>
                    </div>
                </div>
                <div class="_item">
                    <div class="_icon"><img src="<?php echo IMAGE_URL . '/sapphire_sea/s1_icon3.png'?>"></div>
                    <div class="_text">
                        <p>trả góp thời hạn <br> lên đến</p>
                        <p>35<span>năm</span></p>
                    </div>
                </div>
            </div>
        </div>
        
    </section>
    <section class="sps_s2">
        <div class="_label">
            <div class="_small">phân khu Sapphire by sea</div>
            <div class="_big">TRÁI TIM TRỌN VẸN HƠI THỞ BIỂN - HỒ</div>
        </div>
        <div class="_slide_intro">
            <div class="swiper-container js_slide_sps_s2">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="_img" style="background-image: url('<?php echo IMAGE_URL . '/sapphire_sea/s2_img1.png'?>');">
                            <img class="_decor" src="<?php echo IMAGE_URL . '/sapphire_sea/s2_decor.png'?>">
                        </div>
                        <div class="_title">CĂN HỘ VIEW TRIỆU ĐÔ</div>
                        <div class="_text">
                            <ul>
                                <li>Tầm nhìn đắt giá hướng về biển hồ nước mặn lớn nhất Việt Nam (6,1ha) và hồ nước ngọt nhân tạo trải cát trắng lớn nhất Việt Nam (24,5ha).</li>
                                <li>Xung quanh phân khu đều là những khu thấp tầng cho chủ nhân căn hộ tầm nhìn Panorama - khẳng định đẳng cấp sang trọng.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="_img" style="background-image: url('<?php echo IMAGE_URL . '/sapphire_sea/s2_img2.png'?>');">
                            <img class="_decor" src="<?php echo IMAGE_URL . '/sapphire_sea/s2_decor.png'?>">                          
                        </div>
                        <div class="_title">MỘT BƯỚC CHÂN CHẠM NGÀN TIỆN ÍCH</div>
                        <div class="_text">
                            <ul>
                                <li>Sapphire 1 được quy hoạch như một thành phố thu nhỏ với đầy đủ tiện ích dành riêng cho cư dân ngay dưới tòa nhà: khu nướng BBQ; Công viên xanh; sân thể thao; bể bơi ngoài trời; TTTM-DV; ...</li>
                                <li>Điểm nhấn ấn tượng ngay trong nội khu với cây ánh sáng bồ công anh, bể bơi Cá voi và Hồ Mặt Trăng, mang đến trải nghiệm cuộc sống như ở resort cho cư </li>
                            </ul>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="_img" style="background-image: url('<?php echo IMAGE_URL . '/sapphire_sea/s2_img3.png'?>');">
                            <img class="_decor" src="<?php echo IMAGE_URL . '/sapphire_sea/s2_decor.png'?>">
                        </div>
                        <div class="_title">MÔI TRƯỜNG GIÁO DỤC TINH HOA</div>
                        <div class="_text">
                            <ul>
                                <li>Cha mẹ hoàn toàn yên tâm trao cho con một môi trường giáo dục tinh hoa với hệ thống trường học Vinschool từ mầm non tới PTLC; trường đại học VinUni đẳng cấp</li>
                            </ul>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="_img" style="background-image: url('<?php echo IMAGE_URL . '/sapphire_sea/s2_img4.png'?>');">
                            <img class="_decor" src="<?php echo IMAGE_URL . '/sapphire_sea/s2_decor.png'?>"> 
                        </div>
                        <div class="_title">TỌA ĐỘ TRUNG TÂM</div>
                        <div class="_text">
                            <p>Tọa độ trung tâm chính giữa Thành phố biển hồ Ocean Park, tiếp giáp đại lộ 52m huyết mạch</p>
                            <ul>
                                <li>Chỉ còn 5 phút từ Aeon Mall tới Vinhomes Ocean Park</li>
                                <li>Chỉ 10 phút từ cầu Thanh Trì rẽ phải vào cao tốc Hà Nội - Hải Phòng đến đại đô thị</li>
                                <li>Chỉ còn 15 phút từ Vinhomes Times City qua cầu Vĩnh Tuy tới đại đô thị</li>
                                <li>Chỉ còn 25 phút từ Hồ Hoàn Kiếm di chuyển theo hướng cầu Chương Dương hoặc cầu Vĩnh Tuy, đi thẳng đường Cổ Linh vào cao tốc Hà Nội - Hải Phòng đến đại đô thị</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-button-next btn_next"><img src="<?php echo IMAGE_URL . '/sapphire_sea/btn_next.png'?>"></div>
        <div class="swiper-button-prev btn_prev"><img src="<?php echo IMAGE_URL . '/sapphire_sea/btn_next.png'?>"></div>
        <a href="#" class="btn_register">ĐĂNG KÝ NHẬN BẢNG HÀNG MỚI NHẤT</a>
    </section>
    <section class="sps_s3">
        <div class="_label">Căn hộ mẫu Vinhomes Ocean Park</div>
        <div class="_slide_img">
            <div class="swiper-container js_slide_sps_s3">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="_img" style="background-image: url('<?php echo IMAGE_URL . '/sapphire_sea/s3_img1.png'?>');"></div>
                    </div>
                    <div class="swiper-slide">
                        <div class="_img" style="background-image: url('<?php echo IMAGE_URL . '/sapphire_sea/s3_img2.png'?>');"></div>
                    </div>
                    <div class="swiper-slide">
                        <div class="_img" style="background-image: url('<?php echo IMAGE_URL . '/sapphire_sea/s3_img3.png'?>');"></div>
                    </div>
                    <div class="swiper-slide">
                        <div class="_img" style="background-image: url('<?php echo IMAGE_URL . '/sapphire_sea/s3_img4.png'?>');"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-pagination panigation_def"></div>
        <div class="swiper-button-next btn_next"><img src="<?php echo IMAGE_URL . '/sapphire_sea/btn_next.png'?>"></div>
        <div class="swiper-button-prev btn_prev"><img src="<?php echo IMAGE_URL . '/sapphire_sea/btn_next.png'?>"></div>
        <a href="#" class="btn_register">ĐĂNG KÝ THAM QUAN NHÀ MẪU MIỄN PHÍ</a>
    </section>
    <section class="sps_s5">
        <div class="_label"><span>VỊ TRÍ HUYẾT MẠCH</span> KẾT NỐI 3 KHU KINH TẾ <br> TRỌNG ĐIỂM Hà Nội - Hải Phòng - Hưng Yên</div> 
        <div class="_video">
            <iframe src="https://www.youtube.com/embed/hf996r5NQ3o" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
        </div>
        <div class="_intro_summary">
            <div class="_item">
                <p><span>5</span> phút</p>
                <p>Từ Aeon Mall tới Vinhomes <br> Ocean Park</p> 
            </div>
            <div class="_item">
                <p><span>10</span> phút</p>
                <p>Từ cầu Thanh Trì rẽ phải vào cao tốc <br> Hà Nội - Hải Phòng đến Đại đô thị</p>
            </div>
            <div class="_item">
                <p><span>15</span> phút</p>
                <p>Từ Vinhomes Times City qua cầu <br> Vĩnh Tuy tới Đại đô thị</p>
            </div>
            <div class="_item">
                <p><span>25</span> phút</p>
                <p>Từ hồ Hoàn Kiếm di chuyển theo <br> hướng cầu Chương Dương hoặc cầu <br> Vĩnh Tuy đến Đại đô thị</p>
            </div>
        </div>
    </section>
    <section class="sps_s6">
    </section>
    <section class="sps_s7">
        <div class="_decor" style="background-image: url('<?php echo IMAGE_URL . '/sapphire_sea/s7_decor.png'?>');"></div>
        <div class="_logo">
            <a href="#"> <img src="<?php echo IMAGE_URL . '/sapphire_sea/s7_logo.png'?>"></a>
        </div>
        <div class="_intro">
            <div class="_item">
                <div class="_title">Liên hệ</div>
                <div class="_content">
                    <p><strong>Địa chỉ:</strong> Gia Lâm, Hà Nội</p>
                    <p><a href="tel: 19001018"><strong>Hotline:</strong> 19001018</a></p>
                    <p><a href="mailto: cskhonline@Vinhomes.vn"><strong>Email:</strong> cskhonline@Vinhomes.vn</a></p>
                    <p><strong>Địa chỉ giao dịch:</strong> <br>TT GD BDS Times City – TL13, Tầng B1, TTTM Vincom Megamall, 458 Minh Khai, P. Vĩnh Tuy, Q. Hai Bà Trưng, Hà Nội.</p>
                    <p><a href="tel: 02439328328">02439328328</a></p>
                </div>
            </div>
            <div class="_item">
                <div class="_title">CHÚ Ý</div>
                <div class="_content">
                    * Thông tin, hình ảnh, các tiện ích trên website chỉ mang tính chất tương đối và có thể được điều chỉnh theo quyết định của Chủ đầu tư tại từng thời điểm đảm bảo phù hợp quy hoạch và thực tế thi công Dự án. Các thông tin, cam kết chính thức sẽ được quy định cụ thể tại Hợp đồng mua bán. Việc quản lý, vận hành và kinh doanh của khu đô thị sẽ theo quy định của Ban quản lý.
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($){
        var js_slide_sps_s1 = new Swiper('.js_slide_sps_s1',{
            speed: 1200,
            pagination: {
                el: '.sps_s1 .swiper-pagination',
                clickable: true,
            },
        });
        var js_slide_sps_s2 = new Swiper('.js_slide_sps_s2',{
            speed: 1200,
            slidesPerView: 2,
            spaceBetween: 16,
            loop: true,
            navigation: {
                nextEl: '.sps_s2 .btn_next',
                prevEl: '.sps_s2 .btn_prev',
            },
            // pagination: {
            //     el: '.sps_s1 .swiper-pagination',
            //     clickable: true,
            // },
        });
        var js_slide_sps_s3 = new Swiper('.js_slide_sps_s3',{
            speed: 1200,
            slidesPerView: 2,
            spaceBetween: 16,
            loop: true,
            pagination: {
                el: '.sps_s3 .swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.sps_s3 .btn_next',
                prevEl: '.sps_s3 .btn_prev',
            },
        });
    });
</script>
<?php get_footer(); ?>
<style>
    header, footer{
        display: none;
    }
    
</style>