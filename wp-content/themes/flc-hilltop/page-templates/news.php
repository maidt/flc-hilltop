<?php //Template Name: news 
    get_header();
?>
<?php
$banner = get_field('banner');
?>
<div class="banner_page" style="background-image: url('<?php echo $banner; ?>')">
    <div class="_decor">
        <img src="<?php echo IMAGE_URL .'/homes/decor_banerpage.png' ?>" alt="">
    </div>
    <h2>Tin tức</h2>
</div>
<div class="news">
    <?php
    
        $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
        $articles = tu_get_article_with_pagination($paged, 7);
        $pagination = tu_paginate_links($articles);
    ?>
    <?php if ( $articles->have_posts() ) : ?>
        <?php while ( $articles->have_posts() ) : $articles->the_post(); ?>
        <?php
        $id = get_the_ID();
        $title = get_the_title($id);
        $thumbnail_src = has_post_thumbnail( $id ) ? tu_get_post_thumbnail_src_by_post_id( $id, '' ) : THEME_CHILD_ASSETS . '/images/homes/news_img.png';
        $permalink = get_permalink($id);
        $excerpt = wp_trim_words(get_the_excerpt($id), 25);
        ?>
        <div class="_item">
            <a href="<?php echo $permalink; ?>">
                <div class="_img" style="background-image: url('<?php echo $thumbnail_src; ?>')"></div>
                <div class="_content">
                    <h3><?php echo $title; ?></h3>
                    <div class="_time"><img src="<?php echo IMAGE_URL .'/homes/clock.png' ?>" alt=""><span><?php echo the_time('d/m/Y'); ?></span></div>
                    <div class="_txt">
                        <?php echo $excerpt; ?>
                    </div>
                    <div class="_see_detail"><span>Xem chi tiết</span></div>
                    <div class="_drums">
                        <img src="<?php echo IMAGE_URL .'/homes/new_drums.png' ?>" alt="">
                    </div>
                </div>
            </a>
        </div>
        <?php endwhile; ?>
    <?php endif; ?>   
</div>
<div class="___panigator">
    <?php echo $pagination; ?>
</div>
<?php include_once(get_template_directory() . '/partials/homes/section_8.php'); ?>
<?php include_once(get_template_directory() . '/partials/social.php'); ?>
<?php get_footer(); ?>