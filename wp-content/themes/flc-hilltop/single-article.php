<?php get_header(); ?>

<?php while( have_posts() ) : the_post();?>
<?php
	$post_curent_id = get_the_ID();
    $date = get_the_date('d/m/Y', $post_curent_id);
    $hyperlink = get_permalink($post_curent_id);
    $thumbnail = has_post_thumbnail( $post_curent_id ) ? tu_get_post_thumbnail_src_by_post_id( $post_curent_id, '' ) : '' ;
    $article_relate = tu_get_article_with_pagination( 1, 5, array('post__not_in' => array($post_curent_id)));
?>
<div class="banner_page" style="background-image: url('<?php echo $thumbnail; ?>')">
    <div class="_decor">
        <img src="<?php echo IMAGE_URL .'/homes/decor_banerpage.png' ?>" alt="">
    </div>
    <h2>Tin tức</h2>
</div>
<div class="new_detail">
    <div class="_left">
        <div class="_time"><img src="<?php echo IMAGE_URL .'/homes/clock.png' ?>" alt=""><span><?php echo $date; ?></span></div>
        <h2><?php the_title(); ?></h2>
        <div class="_excerpt">
            <?php the_excerpt(); ?>
        </div>
        <div class="_the_content">
            <?php the_content(); ?>
        </div>
    </div>
    <div class="_right">
        <div class="_see_agency">
            <a href="<?php echo bloginfo('url') ?>/san-pham">
                <img src="<?php echo IMAGE_URL .'/homes/new_agency.png' ?>" alt="">
                <p class="_txt">Xem mặt bằng dự án</p>
            </a>
        </div>

        <div class="_video_project">
            <div class="_title">
                Video dự án
            </div>
            <?php
                $video_project = get_field('video_project', 'option');
            ?>
            <?php foreach ($video_project as $list) { ?>
                <div class="_content">
                    <img src="<?php echo $list['img']; ?>" alt="">
                    <a class="__play" data-fancybox href="<?php echo $list['link_video']; ?>">
                        <img src="<?php echo IMAGE_URL . '/homes/play.png' ?>" alt="">
                    </a>
                </div>
            <?php } ?>
        </div>
        <div class="_related_news">
            <div class="_title">Các bài viết khác</div>
            <div class="_list_news">
                <?php if ( $article_relate->have_posts() ) : ?>
                    <?php while ( $article_relate->have_posts() ) : $article_relate->the_post(); ?>
                    <?php
                    $id = get_the_ID();
                    $title = wp_trim_words(get_the_title($id), 12);
                    $thumbnail_src = has_post_thumbnail( $id ) ? tu_get_post_thumbnail_src_by_post_id( $id, '' ) : THEME_CHILD_ASSETS . '/images/homes/news_img.png';
                    $permalink = get_permalink($id);
                    ?>
                    <a href="<?php echo $permalink; ?>">
                        <div class="_img" style="background-image: url('<?php echo $thumbnail_src; ?>')"></div>
                        <div class="_txt">
                            <h3><?php echo $title; ?></h3>
                            <p>Xem chi tiết</p>
                        </div>
                    </a>
                    <?php endwhile; ?>
                <?php endif; ?>   
            </div>
        </div>
    </div>
</div>
<?php endwhile; ?>
<?php include_once(get_template_directory() . '/partials/homes/section_8.php'); ?>
<?php include_once(get_template_directory() . '/partials/social.php'); ?>

<?php get_footer(); ?>