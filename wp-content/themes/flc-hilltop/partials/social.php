<?php
    $footer = get_field('footer', 'option');
?>
<div class="social_pro__all">
    <a href="<?php echo $footer['link_facebook'] ?>" class="_item" data-aos="fade-up">
        <div class="__icon"><img src="<?php echo IMAGE_URL . '/homes/fb.png' ?>" alt=""></div class="__icon">
    </a>
    <a href="<?php echo $footer['link_email'] ?>" class="_item" data-aos="fade-down">
        <div class="__icon"><img src="<?php echo IMAGE_URL . '/homes/email.png' ?>" alt=""></div class="__icon">
    </a>
    <a href="tel:<?php echo $footer['hotline_project'] ?>" class="_item">
        <div class="__phone_number" data-aos="fade-left" data-aos-delay="800" data-aos-easing="linear" data-aos-duration="1500" ><?php echo $footer['hotline_project'] ?></div>
        <div class="__icon"><img src="<?php echo IMAGE_URL . '/homes/phone.png' ?>" alt=""></div class="__icon">
    </a>
</div>
