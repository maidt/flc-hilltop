<?php
$h_location = get_field('h_location', 5);
?>
<section class="section_4 js_paralax_location">
    <div class="_title_mobie">
        <div class="home_title">
            <h3 data-aos="fade-right">flc hilltop gia lai</h3>
            <h2 data-aos="fade-right">Vị trí</h2>
        </div>
    </div>
    <div class="_content">
        <div class="_left">
            <div class="home_title">
                <h3 data-aos="fade-right">flc hilltop gia lai</h3>
                <h2 data-aos="fade-right" data-aos-delay="150"><?php echo $h_location['title']; ?></h2>
            </div>
            <div class="_txt" data-aos="fade-right" data-aos-delay="300">
                <?php echo $h_location['content']; ?>
            </div>
            <div class="_map_aos" data-aos="fade-right" data-aos-delay="450"><a target="_blank" href="<?php echo $h_location['link_map']; ?>" class="_map"><img src="<?php echo IMAGE_URL . '/homes/location.png' ?>" alt=""><span>Chỉ đường cho tôi</span></a></div>
        </div>
        <div class="_right">
            <div class="_location wow bounceIn" data-wow-duration="1s">
                <div class="_bg">
                    <div class="__image wow bounceIn" data-wow-duration="1s" data-wow-delay="1s">
                        <img src="<?php echo $h_location['img']; ?>" alt="">
                    </div>
                    <div class="__list_tool">
                        <?php
                        $i=8;
                        $y=0;
                        foreach ($h_location['location_gold'] as $list) {
                            $i++;
                            $y++;
                            ?>
                            <div class="_item __paralax_location <?php echo $y == 1 ? 'is_active' : ''; ?>" data-depth="0.5">
                                <div class="_icon" data-aos="fade" data-aos-anchor="._location" data-aos-delay="<?php echo $i; ?>00">
                                    <img src="<?php echo IMAGE_URL . '/homes/lucgiac.png' ?>" alt="">
                                    <img src="<?php echo $list['icon']; ?>" alt="">
                                </div>
                                <div data-aos="fade" data-aos-anchor="._location" data-aos-delay="<?php echo $i; ?>00">
                                    <div class="_discription">
                                        <span><?php echo $list['name']; ?></span>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="_elephants">
        <div class="_drums">
            <img src="<?php echo IMAGE_URL . '/homes/drums.png' ?>" alt="" data-aos="zoom-in">
        </div>
        <div class="_image">
            <img src="<?php echo IMAGE_URL . '/homes/voi.png' ?>" alt="" data-aos="fade-up">
            <img src="<?php echo IMAGE_URL . '/homes/voi_m.png' ?>" alt="">
        </div>
        <div class="_loaf">
            <img src="<?php echo IMAGE_URL . '/homes/s4_may_1.png' ?>" alt="">
        </div>
    </div>
</section>

<script>
    jQuery(document).ready(function ($) {
        var parallaxBox = $('.js_paralax_location');
        var strength = 0.2,
            isMobile = false;

        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
            isMobile = true;
        }


        function parallaxMove(parallaxContainer, x, y, boxWidth, boxHeight) {
            $(parallaxContainer).find('.__paralax_location').each(function () {
                var depth = $(this).data('depth');
                var moveX = ((boxWidth / 2) - x) * (strength * depth);
                var moveY = ((boxHeight / 2) - y) * (strength * depth);
                // console.log(x);
                // console.log(y);
                $(this).css({ transform: "translate3d(" + moveX + "px, " + moveY + "px, 0)" });
                //$(this).removeClass('is-out');
            });
        }


        function resetParallaxPosition(parallaxContainer) {
            $(parallaxContainer).find('.__paralax_location').each(function () {
                $(this).css({ transform: "translate3d( 0, 0, 0 )" });
                //$(this).addClass('is-out');
            });
            event.stopPropagation();
        }

        if (!isMobile) {

            parallaxBox.mousemove(function (event) {
                event.stopPropagation();
                event = event || window.event;
                var x = Math.floor(event.clientX),
                    y = Math.floor(event.clientY),
                    boxWidth = $(this).width(),
                    boxHeight = $(this).height();

                parallaxMove(this, x, y, boxWidth, boxHeight);

            });

            parallaxBox.mouseleave(function (event) {
                if (!$(event.target).is($(this))) {
                    resetParallaxPosition(this);
                }
            });

        } else if (isMobile) {
            // var elem = document.getElementById("view3d");

            // window.addEventListener("deviceorientation", function (event) {
            //     event.stopPropagation();
            //     event = event || window.event;

            //     var rotatedY = Math.min(Math.max(parseInt(Math.floor(event.gamma)), -45), 45),
            //         rotatedX = Math.min(Math.max(parseInt(Math.floor(event.beta)), -45), 45),
            //         boxWidth = parallaxBox.width(),
            //         boxHeight = parallaxBox.height();

            //     var moveX = ((boxWidth / 2) * rotatedY) / 45;
            //     var moveY = ((boxWidth / 2) * rotatedX) / 45;

            //     parallaxMove(parallaxBox, moveX, moveY, boxWidth, boxHeight);

            // });
        }

        if($(window).width() <= 480){
            $('.__list_tool .__paralax_location').click(function(e) {
                $(this).addClass('is_active');
                $(this).siblings().removeClass('is_active');
            });
        };
    });
</script>