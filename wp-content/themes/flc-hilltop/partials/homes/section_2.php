<?php
$h_intro = get_field('h_intro', 5);
?>
<section class="section_2" id="gioi-thieu">
    <div class="_left">
        <div class="home_title">
            <h3 data-aos="fade-up">flc hilltop gia lai</h3>
            <h2 data-aos="fade-up" data-aos-delay="100"> <?php echo $h_intro['title'];?></h2>
        </div>
        <div class="_txt" data-aos="fade-up" data-aos-delay="200">
            <?php echo $h_intro['content_left'];?>
        </div>
        <div class="_video _img" data-aos="fade-up" data-aos-delay="300">
            <img src="<?php echo $h_intro['img_video'];?>" alt="">
            <div class="__transfrom">
                <a class="__play wow tada" data-wow-delay="1s"  data-fancybox href="<?php echo $h_intro['link_youtube'];?>">
                    <img src="<?php echo IMAGE_URL . '/homes/play.png' ?>" alt="">
                </a>
            </div>
        </div>
    </div>
    <div class="_right">
        <div class="_img" data-aos="fade-left" data-aos-delay="100">
            <img src="<?php echo $h_intro['img_right'];?>" alt="">
        </div>
        <div class="_txt" data-aos="fade-left" data-aos-delay="200">
            <?php echo $h_intro['content_right'];?>
        </div>
        <!-- <a href="#" class="see_more" data-aos="fade-left">Xem chi tiết</a> -->
    </div>
</section>