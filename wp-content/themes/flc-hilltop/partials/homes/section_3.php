<?php
$h_product = get_field('h_product', 5);
?>
<section class="section_3">
    <div class="_drums">
        <img src="<?php echo IMAGE_URL . '/homes/s2_drums.png' ?>" alt="">
    </div>
    <div class="_forest">
        <img src="<?php echo IMAGE_URL . '/homes/s3_rung_2.png' ?>" alt="">
        <img class="_decor" src="<?php echo IMAGE_URL . '/homes/decor3_mo.png' ?>" alt="">
    </div>
    <div class="_layer">
        <div class="home_title">
            <h3 data-aos="fade-up">flc hilltop gia lai</h3>
            <h2 data-aos="fade-down">Sản phẩm</h2>
        </div>
        <div class="_content">
            <div class="_item">
                <?php
                $i=0;
                foreach ($h_product['img_list'] as $list) {
                    $i++;
                    if($i == 2)
                        break;
                ?>
                    <div class="_img" style="background-image: url('<?php echo $list; ?>')" data-aos="fade-left" data-aos-offset="300"></div>
                <?php } ?>
                
            </div>
            <div class="_item">
                <div class="_img" data-aos="fade-right" data-aos-offset="300">
                    <?php
                    $i=0;
                    foreach ($h_product['img_list'] as $list) {
                        $i++;
                        if($i == 2){
                    ?>
                        <div class="__bg" style="background-image: url('<?php echo $list; ?>')">
                    <?php }} ?>
                        <div class="swiper-container js_swiper_container_s3">
                            <div class="swiper-wrapper">
                                <?php
                                $i=0;
                                foreach ($h_product['img_list'] as $list) {
                                    $i++;
                                ?>
                                    <div class="swiper-slide" style="background-image: url('<?php echo $list; ?>')"></div>
                                <?php } ?>
                            </div>
                            <div class="swiper-pagination pagination_all"></div>
                        </div>
                    </div>
                </div>
                <div class="_info" data-aos="fade-rigth" data-aos-offset="300">
                    <div class="_label" data-aos="fade-up" data-aos-delay="500"><?php echo $h_product['title'];?></div>
                    <div class="_txt" data-aos="fade-up" data-aos-delay="550">
                       <?php echo $h_product['content'];?>
                    </div>
                    <div data-aos="fade-up" data-aos-delay="600">
                        <a href="<?php echo bloginfo('url') ?>/san-pham" class="_see_more"><span>Xem chi tiết</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    var swiper_s3 = new Swiper('.js_swiper_container_s3.swiper-container', {
        loop: true,
        autoplay: true,
        speed: 1200,
        pagination: {
            el: '.js_swiper_container_s3 .swiper-pagination',
            clickable: true,
        },
    });
  </script>