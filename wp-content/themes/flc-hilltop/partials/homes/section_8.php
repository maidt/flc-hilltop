<section class="section_8">
	<div class="_bg_decor" style="background-image: url('<?php echo IMAGE_URL .'/home_mai/s8_bg.jpg'?>')"></div>
	<div class="_home_contact">
		<div class="_left">
			<div class="home_title">
            	<h3 class="wow bounceInLeft" data-wow-duration="2s">flc hilltop gia lai</h3>
            	<h2 class="wow bounceInLeft" data-wow-duration="2s">Đăng ký nhận tư vấn</h2>
        	</div>
		</div>
		<div class="_right wow bounceInRight">
			<form action="#" method="post" id="js-form-register">
				<input type="hidden" name="action" value="submit_contact">
            	<input type="hidden" name="nonce" value="<?php echo wp_create_nonce('submit_contact'); ?>">
				<div class="all-form-fields">
					<div class="_item">
						<label for="contact_name">Họ tên:</label>
						<input type="text" name="contact_name" id="contact_name" value="" />
					</div>
					<div class="_item">
						<label for="contact_phone">Số điện thoại:</label>
						<input type="tel" name="contact_phone" id="contact_phone" value="" />
					</div>
					<div class="_item">
						<label for="contact_email">Email:</label>
						<input type="email" name="contact_email" id="contact_email" value="" />
					</div>
					<div class="_item">
						<label for="contact_content">Nội dung:</label>
						<textarea  rows="3" name="contact_content" id="contact_content"></textarea>
					</div>
					<div class="_other">
						<div class="g-recaptcha" data-sitekey="6LcT5ccUAAAAAC02mToPe3hK-tNKxFDfnf0NoXp2"></div>
						<button type="submit">Gửi thông tin</button>
					</div>
				</div>
				<div class="_msg_frm"></div>
			</form>
			<script src="https://www.google.com/recaptcha/api.js"></script>
		</div>
	</div>
	<div class="_home_agency">
		<div class="_left">
			<div class="home_title wow bounceInLeft" data-wow-duration="2s">
	            <h2>Danh sách <br> đại lý</h2>
	        </div>
		</div>
		<div class="_right">
			<div class="swiper-container js_swiper_home_agency">
		        <div class="swiper-wrapper">
		        	<?php
						$agency = get_field('agency', 'option');
						$index = 0;
				    ?>
				    <?php foreach ($agency as $list) { $index= $index + .5;?>
	                    <div class="swiper-slide wow bounceInUp" data-wow-duration="2s" data-wow-delay="<?php echo $index; ?>s">
			               <img class="_logo" src="<?php echo $list['logo']; ?>" alt="">
			               <div class="_intro">
			               		<h3><?php echo $list['name']; ?></h3>
			               		<p><a href="tel:<?php echo $list['hotline']; ?>"><?php echo $list['hotline']; ?> </a></p>
			               </div>
			            </div>
	                <?php } ?>
		        </div>
		    </div>
		    <div class="swiper-pagination js_pagination_agency pagination_all"></div>
		    <div class="swiper-button-next js_h_agency_next"><img src="<?php echo IMAGE_URL .'/home_mai/s8_next.png'?>" alt=""></div>
		    <div class="swiper-button-prev js_h_agency_prev"><img src="<?php echo IMAGE_URL .'/home_mai/s8_prev.png'?>" alt=""></div>
		</div>
	</div>
</section>
<script>
    jQuery(document).ready(function($){
        var swiper_h_agency = new Swiper('.js_swiper_home_agency', {
        	slidesPerView: 4,
            speed: 1200,
            spaceBetween: 5,
            autoplay: {
			    delay: 5000,
			},
			navigation: {
		        nextEl: '.js_h_agency_next',
		        prevEl: '.js_h_agency_prev',
		    },
	       	pagination: {
		        el: '.js_pagination_agency',
		        type: 'fraction',
		    },
		    renderFraction: function (currentClass, totalClass) {
			    return '<span class="' + currentClass + '"></span>' +
			            ' / ' +
			            '<span class="' + totalClass + '"></span>';
			},
            breakpoints: {
			    480: {
			       	slidesPerView: 2,
			    },
			    768: {
			       	slidesPerView: 3,
			    }
			}
        });
    });
</script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        
        // form register 
        $('#js-form-register').submit(function (event) {
            event.preventDefault();
            var form = $(this);
            var formButton = form.find('[type="submit"]');
            var formFields = form.find('.all-form-fields');
            var formMessage = form.find('._msg_frm');
            $.ajax({
                url: wp_vars.ADMIN_AJAX_URL,
                type: 'POST',
                dataType: 'json',
                data: form.serialize(),
                beforeSend: function () {
                    formButton.text("Đang gửi ...");
                    formButton.prop('disabled', true);
                },
                complete: function () {
                    formButton.text('Gửi thông tin');
                    formButton.prop('disabled', false);
				},
				// error: function( jqXHR, textStatus, errorThrown ){
                //     console.log( 'The following error occured: ' + jqXHR, textStatus, errorThrown );
                // }
            })
            .done(function (response) {
                formButton.text('Đăng ký');
                formButton.prop('disabled', false);

                // console.log(response);

                if (response.success == true) {
                    formFields.hide();
                    formButton.hide();
                    formMessage.text(response.msg);
                    window.location.href = "http://flchilltopgialai.vn/thank-you/";
                }
                if (response.success == false && response.msg) {
                    formMessage.text(response.msg);
                    // console.log('Subscribe Error!'); 
                }
            })
            .fail(function () {
                // console.log("Submit error!");
                $(this).removeAttr('disabled');
            })
            .always(function () {
                // console.log('Event submit');
            });
        });
    });
    
</script>
