<?php
    $articles = tu_get_article_with_pagination(1, 5);
?>
<section class="section_7">
	<div class="home_title">
	    <h3 data-aos="fade-up">flc hilltop gia lai</h3>
	    <h2 data-aos="fade-up">Tin tức</h2>
	</div>
	<div class="_drums">
        <img src="<?php echo IMAGE_URL . '/homes/drums.png' ?>" alt="">
    </div>
	<div class="_h_article_list">
        <div class="_left js_left_h_article" data-aos="fade-up">
        	<?php if ( $articles->have_posts() ) : $i = 1; ?>
				<?php while ( $articles->have_posts() ) : $articles->the_post(); ?>
				<?php
	            $id = get_the_ID();
	            $title = get_the_title($id);
	            $thumbnail_src = has_post_thumbnail( $id ) ? tu_get_post_thumbnail_src_by_post_id( $id, '' ) : '';
	            $permalink = get_permalink($id);
	            $excerpt = wp_trim_words(get_the_excerpt($id), 23);
				$date = get_the_date('d/m/Y', $id);
				if($i == 1) :
	            ?>
                <a href="<?php echo $permalink; ?>" class="_img" style="background-image: url('<?php echo $thumbnail_src; ?>')"></a>
	            <div class="_text">
	                <h2 class="_title"><a href="<?php echo $permalink; ?>"><?php echo $title; ?></a></h2>
	                <div class="_date"><i class="fa fa-clock-o" aria-hidden="true"></i><?php echo $date; ?></div>
	                <div class="_excerpt"><?php echo $excerpt; ?></div>
	                <a href="<?php echo $permalink; ?>" class="_read_more_def" data-aos="fade-right" data-aos-delay="300">Xem thêm</a>
	            </div> 
            	<?php endif; $i++ ;endwhile; ?>
            	<?php wp_reset_postdata(); ?>
            <?php endif; ?>
            
        </div>
        <div class="_right">
        	<div class="swiper-container js_swiper_h_article">
	            <div class="swiper-wrapper">
					<?php if ( $articles->have_posts() ) : $i = 2; ?>
						<?php while ( $articles->have_posts() ) : $articles->the_post(); ?>
						<?php
			            $id = get_the_ID();
			            $title = get_the_title($id);
			            $thumbnail_src = has_post_thumbnail( $id ) ? tu_get_post_thumbnail_src_by_post_id( $id, '' ) : '';
			            $permalink = get_permalink($id);
			            $excerpt = wp_trim_words(get_the_excerpt($id), 23);
						$date = get_the_date('d/m/Y', $id);
						// $i = $i + 2;
						if($i > 2) :
			            ?>
		            	<div class="swiper-slide" data-aos="fade-up" data-aos-delay="<?php echo $i; ?>">
		                    <a href="<?php echo $permalink; ?>" class="_item">
		                    	<div class="_img" style="background-image: url('<?php echo $thumbnail_src; ?>')"></div>
		                    	<div class="_text">
		                    		<h3 class="_title"><?php echo $title; ?></h3>
			                        <div class="_date"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $date; ?></div>
		                			<div class="_excerpt"><?php echo $excerpt; ?></div>
	                				<div class="_read_more_def">Xem thêm</div>
		                    	</div>
		                    </a>
		                </div> 
		            	<?php endif; $i++; endwhile; ?>
		            	<?php wp_reset_postdata(); ?>
		            <?php endif; ?>
	            </div>
	            <div class="swiper-button-next btn_next_def js_h_article_next" data-aos="fade-right" data-aos-delay="900">Tiếp</div>
		    	<div class="swiper-button-prev btn_prev_def js_h_article_prev" data-aos="fade-left" data-aos-delay="900">Trước</div>
		    </div>
		    <a href="<?php echo bloginfo('url') ?>/tin-tuc" class="_load_all_def" data-aos="fade-left" data-aos-delay="900">Xem tất cả</a>
        </div>
	</div>
	<div class="_h_article_list_mobi">
		<?php
            $articles_mobi = tu_get_article_with_pagination(1, 3);
        ?>
		<?php if ( $articles_mobi->have_posts() ) : ?>
			<?php while ( $articles_mobi->have_posts() ) : $articles_mobi->the_post(); ?>
			<?php
            $id = get_the_ID();
            $title = get_the_title($id);
            $thumbnail_src = has_post_thumbnail( $id ) ? tu_get_post_thumbnail_src_by_post_id( $id, '' ) : '';
            $permalink = get_permalink($id);
            $date = get_the_date('d/m/Y', $id);
            ?>
			<a href="<?php echo $permalink; ?>" class="_item">
				<div class="_img" style="background-image: url('<?php echo $thumbnail_src; ?>')"></div>
				<div class="_text">
	                <div class="_date"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $date; ?></div>
					<h3 class="_title"><?php echo $title; ?></h3>
				</div>
			</a>
			<?php endwhile; ?>
		<?php endif; ?>
		<a href="#" class="_load_all_def">Xem tất cả</a>
	</div>
</section>
<script>
    jQuery(document).ready(function($){
        var swiper_h_article = new Swiper('.js_swiper_h_article', {
            slidesPerView: 2,
	        spaceBetween: 20,
	        // loop: true,
	        speed: 900,
	        navigation: {
		        nextEl: '.js_h_article_next',
		        prevEl: '.js_h_article_prev',
		    },
        });
        //js lay thong tin slide
            // var prev = $('.js_swiper_h_article').find('.swiper-slide-active');
            // var title = $($(prev).find('._title')).text();
            // var date = $($(prev).find('._date')).text();
            // var excerpt = $($(prev).find('._excerpt')).text();
            // var href = $($(prev).find('._item')).attr('href');
            // var img = $($(prev).find('._img')).attr('style');
            // $('.js_left_h_article ._img').attr('style', img);
            // $('.js_left_h_article ._title').text(title);
            // $('.js_left_h_article ._date').text(date);
            // $('.js_left_h_article ._excerpt').text(excerpt);
            // $('.js_left_h_article ._img').attr('href',href);
            // $('.js_left_h_article ._text ._read_more_def').attr('href',href);
            // $('.js_left_h_article ._text ._title a').attr('href',href);

        //js check chuyen dong slide
        // swiper_h_article.on('slideChange', function(){
        //     var prev = $('.js_swiper_h_article').find('.swiper-slide-active');
        //     var title = $($(prev).find('._title')).text();
        //     var date = $($(prev).find('._date')).text();
        //     var excerpt = $($(prev).find('._excerpt')).text();
        //     var href = $($(prev).find('._item')).attr('href');
        //     var img = $($(prev).find('._img')).attr('style');
        //     $('.js_left_h_article ._img').attr('style', img);
        //     $('.js_left_h_article ._title').text(title);
        //     $('.js_left_h_article ._date').text(date);
        //     $('.js_left_h_article ._excerpt').text(excerpt);
        //     $('.js_left_h_article ._img').attr('href',href);
        //     $('.js_left_h_article ._text ._read_more_def').attr('href',href);
        //     $('.js_left_h_article ._text ._title a').attr('href',href);
        // });
    });
</script>