<?php
$library = get_field('library', 5);
$document = get_field('document', 5);

?>
<section class="section_6" id="thu-vien" style="background-image: url('<?php echo IMAGE_URL .'/home_mai/s6_bg.png'?>')">
	<div class="home_title">
	    <h3 data-aos="fade-right">flc hilltop gia lai</h3>
	    <h2 data-aos="fade-left">Thư viện</h2>
	</div>
	<div class="_tab_library" data-aos="fade-up">
		<div class="swiper-container gallery-thumbs">
		    <div class="swiper-wrapper">
		      	<div class="swiper-slide">
		      		<a>Hình ảnh</a>
		      	</div>
		      	<div class="swiper-slide">
		      		<a>Tài liệu</a>
		      	</div>
		    </div>
		</div>
	</div>
	<div class="_inner_wrap" data-aos="fade-up">
		<div class="swiper-container gallery-top">
		    <div class="swiper-wrapper">
		      <div class="swiper-slide">
		      	<div class="_h_gallery" id="">
					<div class="swiper-container js_swiper_h_gallery">
		                <div class="swiper-wrapper">
		                	<?php
			                foreach ($library as $list) {
			                    ?>
			                    <div class="swiper-slide">
			                        <div class="_image" style="background-image: url('<?php echo $list['url']; ?>')">
			                        	<img src="<?php echo $list['url']; ?>" alt="#">
			                        </div>
			                        <div class="_text">
			                        	<div class="_name"><?php echo $list['title']; ?></div>
			                        	<div class="_excerpt"><?php echo $list['caption']; ?></div>
			                        </div>
			                    </div>
			                    <?php
			                }
			                ?>
		                    
		                </div>
		                <div class="swiper-pagination pagination_all js_h_gallery_pagination"></div>
		            </div>
		            <div class="swiper-button-next js_h_gallery_next"><img src="<?php echo IMAGE_URL .'/home_mai/s6_next.png'?>" alt=""></div>
				    <div class="swiper-button-prev js_h_gallery_prev"><img src="<?php echo IMAGE_URL .'/home_mai/s6_prev.png'?>" alt=""></div>
				</div>
		      </div>
		      <div class="swiper-slide">
		      	<div class="_h_document" id="">
					<div class="swiper-container js_swiper_h_document">
		                <div class="swiper-wrapper">
		                	<?php
			                foreach ($document as $list) {
			                    ?>
			                   	<div class="swiper-slide">
			                        <div class="_top">
			                        	<img class="_logo" src="<?php echo IMAGE_URL .'/home_mai/logo_ft.png'?>" alt="">
			                        </div>
			                        <div class="_bottom">
			                        	<div class="_left">
			                        		<?php echo $list['name']; ?>
			                        	</div>
			                        	<a href="<?php echo $list['file']; ?>" download class="_right">
			                        		Tải xuống <img src="<?php echo IMAGE_URL .'/home_mai/download.png'?>" alt="">
			                        	</a>
			                        </div>
			                    </div>
			                    <?php
			                }
			                ?>
		                    
		                </div>
		                <div class="swiper-pagination pagination_all js_h_document_pagination"></div>
		            </div>
		            <div class="swiper-button-next js_h_document_next"><img src="<?php echo IMAGE_URL .'/home_mai/s6_next.png'?>" alt=""></div>
				    <div class="swiper-button-prev js_h_document_prev"><img src="<?php echo IMAGE_URL .'/home_mai/s6_prev.png'?>" alt=""></div>
				</div>
		      </div>
		    </div>
		</div>
	</div>
</section>
<script>
    jQuery(document).ready(function($){
    	var galleryThumbs = new Swiper('.gallery-thumbs', {
		    spaceBetween: 10,
		    slidesPerView: 2,
		    freeMode: true,
		    watchSlidesVisibility: true,
		    watchSlidesProgress: true,
		    speed: 1200,
	    });
	    var galleryTop = new Swiper('.gallery-top', {
	      	spaceBetween: 10,
	      	allowTouchMove: false,
		    speed: 1200,
		    thumbs: {
		        swiper: galleryThumbs
		    }
	    });
	    $('.gallery-thumbs .swiper-slide-active').addClass('is-active');
	    $('.gallery-thumbs .swiper-slide').each(function(index){
		    $(this).click(function(){
		        $('.gallery-thumbs .swiper-slide').removeClass('is-active');
		        $(this).addClass('is-active');
		    });
		});

    	var swiper_h_documenter = new Swiper('.js_swiper_h_document', {
		    slidesPerView: 2,
		    slidesPerColumn: 2,
		    spaceBetween: 30,
		    slidesPerColumnFill: 'row',
		    pagination: {
		        el: '.js_h_document_pagination',
		        clickable: true,
		    },
		    navigation: {
		        nextEl: '.js_h_document_next',
		        prevEl: '.js_h_gallery_prev',
		    },
		    breakpoints: {
		    	480: {
		    		spaceBetween: 25,
			      	slidesPerView: 1,
		    	},
			    1024: {
			      	slidesPerView: 1,
			    }
			}
	    });
        var swiper_h_gallery = new Swiper('.js_swiper_h_gallery', {
           	effect: 'coverflow',
		    grabCursor: true,
		    centeredSlides: true,
	        loop: true,
	        slidesPerView: 'auto',
	        speed: 1200,
	        coverflowEffect: {
		        rotate: 0,
		        stretch: 0,
		        depth: 300,
		        modifier: 1,
		        slideShadows : true,
		    },
	        navigation: {
		        nextEl: '.js_h_gallery_next',
		        prevEl: '.js_h_utilities_prev',
		    },
		    pagination: {
		        el: '.js_h_gallery_pagination',
		        clickable: true,
		    },
		    breakpoints: {
			    1024: {
			      	coverflowEffect: {
				        depth: 200,
				    },
			    }
			}
        });
    });
</script>
