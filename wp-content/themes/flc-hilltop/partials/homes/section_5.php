<?php
$h_utilities = get_field('h_utilities', 5);
?>
<section class="section_5" id="tien-ich">
	<div class="home_title">
	    <h3 data-aos="fade-right">flc hilltop gia lai</h3>
	    <h2 data-aos="fade-left"><?php echo $h_utilities['title']; ?></h2>
	</div>
	<div class="_inner_wrap">
		<div class="swiper-container js_swiper_h_utilities">
            <div class="swiper-wrapper">
            	<?php foreach ($h_utilities['utilities_list'] as $list) { ?>
                   	<div class="swiper-slide">
	                    <div class="_left">
	                    	<div class="_title" data-aos="fade-right" data-aos-delay="500"><?php echo $list['u_title']; ?></div>
	                    	<div class="_content js_aos" data-aos="fade-left">
	                    		<?php echo $list['u_content']; ?>
	                    	</div>
	                    	<div class="_img" data-aos="fade-left" data-aos-anchor=".js_aos">
	                    		<?php 
	                    			$i = 0;
	                    			foreach ($list['album_img'] as $gallery) { 
	                    			$i++;
	                    			if($i > 2)
	                    				break;
	                    		?>
	                    			<div class="_img_item" style="background-image: url('<?php echo $gallery; ?>')"></div>
	                    		<?php } ?>
	                    	</div>
	                    </div>
	                    <div class="_right">
	                    	<?php 
                    			$i = 0;
                    			foreach ($list['album_img'] as $gallery) { 
                    			$i++;
                    			if($i > 2){
                    		?>
                    			<div class="_img_item" style="background-image: url('<?php echo $gallery; ?>')" data-aos="fade-right"></div>
                    		<?php }} ?>
	                    </div>
	                </div>
                <?php } ?>
            	
            </div>
            <div class="swiper-pagination pagination_all js_h_utilities_pagination" data-aos="fade-right"></div>
            <div class="swiper-button-next btn_next_def js_h_utilities_next" data-aos="zoom-in">Tiếp</div>
	    	<div class="swiper-button-prev btn_prev_def js_h_utilities_prev" data-aos="zoom-in">Trước</div>
	    </div>
	</div>
</section>
<script>
    jQuery(document).ready(function($){
        var swiper_h_utilities = new Swiper('.js_swiper_h_utilities', {
            slidesPerView: 1,
	        spaceBetween: 10,
	        loop: true,
	        speed: 1600,
	        // effect: 'cube',
	        navigation: {
		        nextEl: '.js_h_utilities_next',
		        prevEl: '.js_h_utilities_prev',
		    },
		    pagination: {
		        el: '.js_h_utilities_pagination',
		        type: 'fraction',
		    },
		    renderFraction: function (currentClass, totalClass) {
			    return '<span class="' + currentClass + '"></span>' +
			            ' / ' +
			            '<span class="' + totalClass + '"></span>';
			},
		    breakpoints: {
			    1024: {
			      	pagination: {
				        el: '.js_h_utilities_pagination',
				        type: 'bullets',
				    },
			    }
			}
        });
    });
</script>