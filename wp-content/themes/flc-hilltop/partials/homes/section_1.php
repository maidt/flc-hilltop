<section class="section_1">
    <div class="_decor_header">
        <img src="<?php echo IMAGE_URL .'/homes/decor_header.png'?>" alt="">
    </div>
    <div class="swiper-container js_swiper_container_banner">
        <div class="swiper-wrapper">
            <div class="swiper-slide js_paralax_banner_01">
                <div class="_banner" style="background-image: url('<?php echo IMAGE_URL .'/homes/s1_bg1.png'?>')">
                    <div class="_item __paralax_banner" data-depth="0.1">
                        <img src="<?php echo IMAGE_URL .'/homes/bg_banner.png'?>" alt="">
                    </div>
                    <div class="_item __paralax_banner" data-depth="0.15">
                        <img src="<?php echo IMAGE_URL .'/homes/river_moutian_banner.png'?>" alt="">
                    </div>
                    <div class="_item __paralax_banner" data-depth="-0.1">
                        <img src="<?php echo IMAGE_URL .'/homes/river_banner.png'?>" alt="">
                    </div>
                    <div class="_item __paralax_banner" data-depth="-0.2">
                        <img src="<?php echo IMAGE_URL .'/homes/tree_banner.png'?>" alt="">
                    </div>
                </div>
                <div class="_text">
                    <h2  data-aos="fade-right" data-aos-delay="200">cánh rừng thông</h2>
                    <div class="__info">
                        <p data-aos="fade-up" data-aos-delay="300">trải dài trên vùng đất nâu đỏ,</p>
                        <p data-aos="fade-up" data-aos-delay="400">uốn lượn theo</p>
                        <p data-aos="fade-up" data-aos-delay="500">Triền hồ mênh mông và phẳng lặng</p>
                    </div>
                </div>
            </div>
            <div class="swiper-slide js_paralax_banner_01">
                <div class="_banner">
                    <div class="_img __paralax_banner" data-depth="0.1" style="background-image: url('<?php echo IMAGE_URL .'/homes/s1_bg.png'?>')"></div>
                    <div class="_human __paralax_banner" data-depth="-0.15">
                        <img src="<?php echo IMAGE_URL .'/homes/human.png'?>" alt="">
                    </div>
                </div>
                <div class="_text">
                    <h2>những buôn làng truyền thống giàu bản sắc</h2>
                    <div class="__info">
                        <p>tồn tại hàng trăm năm trong lòng đô thị</p>
                        <p>với không gian lễ hội, không gian văn hóa cồng chiêng</p>
                        <p>in đậm dấu ấn dân tộc</p>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="_banner" style="background-image: url('<?php echo IMAGE_URL .'/homes/s1_bg3.png'?>')"></div>
                <h1>flc hilltop gia lai</h1>
                <div class="_text">
                    <div class="__info">
                        <p>Phố núi Pleiku nguyên sơ  và thơ mộng đang trở mình từng ngày để vươn lên thành trung tâm kinh tế lớn nhất Tây Nguyên, và là địa điểm thực hiện ước muốn kiến tạo</p>
                    </div>
                    <h2>Một tổ hợp đô thị hiện đại,<br>Một trung tâm giải trí mới</h2>
                    <div class="_label">trên mảnh đất sử thi hào hùng</div>
                </div>
                <div class="_decor">
                    <img src="<?php echo IMAGE_URL .'/homes/s1_decor3.png'?>" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="_decor_bt">
        <img src="<?php echo IMAGE_URL .'/homes/s1_decor.png'?>" alt="">
    </div>
    <div class="swiper-pagination js_pagination_banner pagination_all"></div>
    <div class="_scroll_down" data-aos="zoom-in" data-aos-anchor=".section_1">
        <span>Scroll down</span>
    </div>
</section>
<script>
    jQuery(document).ready(function($){
        var swiper_banner = new Swiper('.swiper-container.js_swiper_container_banner', {
            speed: 1000,
            autoplay: {
                delay: 7000,
            },
            pagination: {
                el: '.swiper-pagination.js_pagination_banner',
                clickable: true,
            },
        });
    });
</script>

<script>
    jQuery(document).ready(function ($) {
        var parallaxBox = $('.js_paralax_banner_01');
        var strength = 0.2,
            isMobile = false;

        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
            isMobile = true;
        }


        function parallaxMove(parallaxContainer, x, y, boxWidth, boxHeight) {
            $(parallaxContainer).find('.__paralax_banner').each(function () {
                var depth = $(this).data('depth');
                var moveX = ((boxWidth / 2) - x) * (strength * depth);
                var moveY = ((boxHeight / 2) - y) * (strength * depth);
                // console.log(x);
                // console.log(y);
                $(this).css({ transform: "translate3d(" + moveX + "px, " + moveY + "px, 0)" });
                //$(this).removeClass('is-out');
            });
        }


        function resetParallaxPosition(parallaxContainer) {
            $(parallaxContainer).find('.__paralax_banner').each(function () {
                $(this).css({ transform: "translate3d( 0, 0, 0 )" });
                //$(this).addClass('is-out');
            });
            event.stopPropagation();
        }

        if (!isMobile) {

            parallaxBox.mousemove(function (event) {
                event.stopPropagation();
                event = event || window.event;
                var x = Math.floor(event.clientX),
                    y = Math.floor(event.clientY),
                    boxWidth = $(this).width(),
                    boxHeight = $(this).height();

                parallaxMove(this, x, y, boxWidth, boxHeight);

            });

            parallaxBox.mouseleave(function (event) {
                if (!$(event.target).is($(this))) {
                    resetParallaxPosition(this);
                }
            });

        } else if (isMobile) {
            var elem = document.getElementById("view3d");

            window.addEventListener("deviceorientation", function (event) {
                event.stopPropagation();
                event = event || window.event;

                var rotatedY = Math.min(Math.max(parseInt(Math.floor(event.gamma)), -45), 45),
                    rotatedX = Math.min(Math.max(parseInt(Math.floor(event.beta)), -45), 45),
                    boxWidth = parallaxBox.width(),
                    boxHeight = parallaxBox.height();

                var moveX = ((boxWidth / 2) * rotatedY) / 45;
                var moveY = ((boxWidth / 2) * rotatedX) / 45;

                parallaxMove(parallaxBox, moveX, moveY, boxWidth, boxHeight);

            });
        }
    });
</script>