<div class="popup_register js_popup_register">
	<div class="_home_contact">
		<div class="_drums">
	        <img src="<?php echo IMAGE_URL .'/homes/drums.png'?>" alt="">
	    </div>
		<div class="_close"><img src="<?php echo IMAGE_URL .'/home_mai/close.png'?>" alt=""></div>
		<div class="_left">
			<div class="home_title">
            	<h3>flc hilltop gia lai</h3>
            	<h2>Đăng ký nhận tư vấn</h2>
        	</div>
        	<div class="_img" style="background-image: url(<?php echo IMAGE_URL .'/homes/news_img.png'?>);">
        	</div>
		</div>
		<div class="_right">
			<form action="#" method="post" id="js-form-popup">
				<input type="hidden" name="action" value="submit_contact">
            	<input type="hidden" name="nonce" value="<?php echo wp_create_nonce('submit_contact'); ?>">
				<div class="all-form-fields">
					<div class="_item">
						<label for="contact_name">Họ tên:</label>
						<input type="text" name="contact_name" id="contact_name" value="" />
					</div>
					<div class="_item">
						<label for="contact_phone">Số điện thoại:</label>
						<input type="tel" name="contact_phone" id="contact_phone" value="" />
					</div>
					<div class="_item">
						<label for="contact_email">Email:</label>
						<input type="email" name="contact_email" id="contact_email" value="" />
					</div>
					<div class="_item">
						<label for="contact_content">Nội dung:</label>
						<textarea  rows="3" name="contact_content" id="contact_content"></textarea>
					</div>
					<div class="_other">
						<div class="g-recaptcha" data-sitekey="6LcT5ccUAAAAAC02mToPe3hK-tNKxFDfnf0NoXp2"></div>
						<button type="submit">Gửi thông tin</button>
					</div>
				</div>
				<div class="_msg_frm"></div>
			</form>
			<script src="https://www.google.com/recaptcha/api.js"></script>
		</div>
	</div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        
        // form register 
        $('#js-form-popup').submit(function (event) {
            event.preventDefault();
            var form = $(this);
            var formButton = form.find('[type="submit"]');
            var formFields = form.find('.all-form-fields');
            var formMessage = form.find('._msg_frm');
            $.ajax({
                url: wp_vars.ADMIN_AJAX_URL,
                type: 'POST',
                dataType: 'json',
                data: form.serialize(),
                beforeSend: function () {
                    formButton.text("Đang gửi ...");
                    formButton.prop('disabled', true);
                },
                complete: function () {
                    formButton.text('Gửi thông tin');
                    formButton.prop('disabled', false);
				},
            })
            .done(function (response) {
                formButton.text('Đăng ký');
                formButton.prop('disabled', false);
                if (response.success == true) {
                    formFields.hide();
                    formButton.hide();
                    formMessage.text(response.msg);
                    window.location.href = "http://flchilltopgialai.vn/thank-you/";
                }
                if (response.success == false && response.msg) {
                    formMessage.text(response.msg);
                }
            })
            .fail(function () {
                $(this).removeAttr('disabled');
            })
            .always(function () {
                console.log('Event submit');
            });
        });
    });
    
</script>