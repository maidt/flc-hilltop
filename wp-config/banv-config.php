<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

/**
 * Use this to set site url
 *
define('WP_HOME','http://example.com');
define('WP_SITEURL','http://example.com');
 */

/**
 * Use this to replace base url in database:
 *
UPDATE wp_posts SET post_content = REPLACE(post_content,'http://old','http://new');
UPDATE wp_options SET option_value = REPLACE(option_value,'http://old','http://new');
 */

/** The name of the database for WordPress */
define('DB_NAME', 'freelancer_flc_hilltop');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/** Disallow admin to edit plugins, theme */
define( 'DISALLOW_FILE_EDIT', true );
define( 'DISALLOW_FILE_MODS', true );

/** Disable auto update core */
define( 'WP_AUTO_UPDATE_CORE', false );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 *
 * Use this to generate key: https://api.wordpress.org/secret-key/1.1/salt/
 */
define('AUTH_KEY',         ')k-HgmOj1X.HyUN>P(jpQ0+h!3|SC}h5]PPJ[FX@So0lcHiUk/+*)HU#{FYd/Nk]');
define('SECURE_AUTH_KEY',  'oWZ+lm(6<5o!XP*!};<cyi4IJQU%Av+-oIF<(&IA%C;+%-F3-CFe=.w}z1-_MB/-');
define('LOGGED_IN_KEY',    't.K7wc,wAI!2RDg3`F/LLm@b[8.J|]10[=|fW&-T[2[gb1su<+noz6-;[%fQsiqs');
define('NONCE_KEY',        'C$(2k{7B>vDqkJ*a.[$7&|IC3v3d?%RxO-_L;E@Jl3I+fXRh4Mp$cMICE4o&-ULP');
define('AUTH_SALT',        '(C+]T%8~p#5)w^xJd,:|aV#ZVhM*RVkJsVx~F*v$@;%NZ-|*O7u}a/muQ_@?~,t0');
define('SECURE_AUTH_SALT', 'y:>#$?v1uA%3Gi&j%!4^7uW^?4)2d+`UpjGcEHa~rz+zx m`q-b6{gA@Fu87Ks-|');
define('LOGGED_IN_SALT',   'atLA630:+sDx<!|htaR]3 k+^RsHXt_b{q$-hH+fq{X-]w`bdLR?xY[&^#*d<:_w');
define('NONCE_SALT',       '5.qVhW Ia(W@^|@HfQ@s+XEWH?/hCK8MaC;}pZ9N#K#JlkKsuq;ues>zb0jFUA,N');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');